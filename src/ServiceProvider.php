<?php namespace ShaneDaniels\Support;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;
use ReflectionClass;

/**
 * Class ServiceProvider
 * @package Mango\Support
 */
abstract class ServiceProvider extends IlluminateServiceProvider {

    /**
     * Path to our vendors views, relative to
     * the resources path
     *
     * @var string
     */
    protected $viewsPath = 'resources/views';

    /**
     * Path to our vendor's translations, relative to
     * the resources path
     *
     * @var string
     */
    protected $translationPath = 'resources/lang';

    /**
     * Path to our vendor's configs, relative to
     * the resources path
     *
     * @var string
     */
    protected $configPath = 'config';

    /**
     * Relative path to the application's vendor views.
     * This is relative to the application's root.
     *
     * @var string
     */
    protected $appViewsPath = 'resources/views/vendor';

    /**
     * Relative path to the application's vendor views.
     * This is relative to the application's root.
     *
     * @var string
     */
    protected $appTranslationPath = 'resources/lang/vendor';

    /**
     * Register our vendors namespaces and resources
     *
     * @param $vendor
     * @param null $namespace
     * @param string $relPath
     */
    public function package($vendor, $namespace = null, $relPath = '/../')
    {
        $namespace = str_replace(['/', '-', '_'], '.', $namespace ?: $vendor);

        // get the path to the
        $path = $this->guessPackagePath();

        $configPath = realpath($path . $relPath . $this->configPath);

        $this->mergeAndPublishConfig($namespace, $configPath);

        $translationPath = realpath($path . $relPath . $this->translationPath);
        $this->mergeAndPublishTranslations($vendor, $translationPath);

        $viewsPath = realpath($path . $relPath . $this->viewsPath);
        $this->mergeAndPublishViews($vendor, $viewsPath);
    }

    /**
     * @param $vendor
     * @param $path
     */
    public function mergeAndPublishConfig($vendor, $path)
    {
        // if the path doesn't exist, then there is no reason
        // for use to continue.
        if ( $path === false && ! is_dir($path)) return;

        $files = $this->app['files']->files(realpath($path));

        $configsToPublish = [];

        foreach ($files as $file)
        {
            $baseFilename = basename($file, '.php');

            $namespace = $vendor;

            if ($baseFilename != 'config')
            {
                $namespace = $vendor . '.' . $baseFilename;
            }

            $this->mergeConfigFrom($file, $namespace);

            $configsToPublish = array_merge($configsToPublish, [ $file => config_path($namespace . '.php')]);
        }

        if ( ! empty($configsToPublish))
        {
            $this->publishes($configsToPublish);
        }
    }

    /**
     * Merges our packages views and adds our view path
     * to the package's publisher.
     *
     * @param $namespace
     * @param $path
     */
    public function mergeAndPublishViews($namespace, $path)
    {
        if ($this->app['files']->isDirectory($path))
        {
            $this->loadViewsFrom($path, $namespace);

            $this->publishes([$path => $this->getVendorPath('views', $namespace)]);
        }
    }

    /**
     * Merges our packages translations and adds our translation
     * path to the package's publisher.
     *
     * @param $namespace
     * @param $path
     */
    public function mergeAndPublishTranslations($namespace, $path)
    {
        if ($this->app['files']->isDirectory($path))
        {
            $this->loadTranslationsFrom($path, $namespace);

            $this->publishes([$path => $this->getVendorPath('translation', $namespace)]);
        }
    }

    /**
     * Guess the vendor path for the provider.
     *
     * @return string
     */
    public function guessPackagePath()
    {
        $path = (new ReflectionClass($this))->getFileName();

        return realpath(dirname($path));
    }

    /**
     * Returns the path to the app's vendor directories for
     * the given type.
     *
     * @param $type
     * @param $vendor
     * @return string
     */
    protected function getVendorPath($type, $vendor)
    {
        $method = 'app' . studly_case($type) . 'Path';

        return base_path($this->$method . DIRECTORY_SEPARATOR. $vendor);
    }
}